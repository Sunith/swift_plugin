var exec = require('cordova/exec');

exports.echo = function(arg0, success, error) {
  cordova.exec(success, error, 'ModusEchoSwift', 'echo', [arg0]);
};

exports.echojs = function(arg0, success, error) {
  if (arg0 && typeof(arg0) === 'string' && arg0.length > 0) {
    success(arg0);
  } else {
    error('Empty message!');
  }
};

exports.printShit = function() {
    console.log("PEACE");
};

exports.getSimplPayload = function(success) {
    cordova.exec(success , null , 'ModusEchoSwift', 'getSimplPayload',[]);
};

exports.logout = function(arg) {
  cordova.exec(null , null , 'ModusEchoSwift', 'logout', [arg]);
};

exports.makeZetaPayment = function(arg,success) {
    cordova.exec(success , null , 'ModusEchoSwift', 'makeZetaPayment', [arg]);
};

exports.showWallets = function() {
  cordova.exec(null , null , 'ModusEchoSwift', 'showWallets',[]);
};

exports.getAccessToken = function(success) {
    cordova.exec(success , null , 'ModusEchoSwift', 'getAccessToken',[]);
};

exports.getToken = function (token){
    console.log(token);
};

exports.showHelp = function() {
  cordova.exec(null , null , 'ModusEchoSwift', 'showHelp',[]);
};

exports.showHelpdesk = function(arg) {
    cordova.exec(null , null , 'ModusEchoSwift', 'showHelpdesk',[arg]);
};

exports.navigateToLink = function(arg) {
    cordova.exec(null , null , 'ModusEchoSwift', 'navigateToLink',[arg]);
};

exports.goHome = function() {
    console.log("wasssuppppppp!!!!");
    console.log("Test!!!!");
    // angular.injector(['ng', 'cafeApp']).get("Config").goHome();
    goHome();
};
