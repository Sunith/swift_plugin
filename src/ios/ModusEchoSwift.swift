//
//  ModusEchoSwift.swift
//  MyApp
//
//  Created by Sunith B on 18/04/18.
//

import Foundation
import SwiftKeychainWrapper
import SimplFingerPrint

@objc(ModusEchoSwift) class ModusEchoSwift : CDVPlugin {

        var command : CDVInvokedUrlCommand?

    @objc(echo:)
        func echo(command: CDVInvokedUrlCommand) {

            var pluginResult = CDVPluginResult(
                status: CDVCommandStatus_ERROR
            )

            let msg = command.arguments[0] as? String ?? ""

            //        (self.viewController as! MainViewController).navigationController?.popToRootViewController(animated: true)

            //        if msg.count > 0 {
            //            let toastController: UIAlertController =
            //                UIAlertController(
            //                    title: "",
            //                    message: msg,
            //                    preferredStyle: .alert
            //            )
            //            self.viewController?.present(
            //                toastController,
            //                animated: true,
            //                completion: nil
            //            )
            //            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            //                toastController.dismiss(
            //                    animated: true,
            //                    completion: nil
            //                )
            //            }

            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: msg
            )

            //        }

            self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
            )

        }

        @objc(logout:)
        func logout(command: CDVInvokedUrlCommand) {
            UserDefaults.standard.set(nil, forKey: "CURRENT_USER")
            let _ : Bool = KeychainWrapper.standard.removeObject(forKey: "ACCESS_TOKEN")
            let _ : Bool = KeychainWrapper.standard.removeObject(forKey: "MOBILE_NO")
            let _ : Bool = KeychainWrapper.standard.removeObject(forKey: "EMAIL")
            if !((UIApplication.shared.delegate as! AppDelegate).window.rootViewController is DashboardContainerViewController) {
                let rootNavigation = UIStoryboard.onboarding.instantiateViewController(withIdentifier: "OnboardingNavigationController")
                UIApplication.shared.keyWindow?.rootViewController = rootNavigation
                let msg = command.arguments[0] as? String ?? ""
                if msg == "changePassword" {
                    Helper.showAlert(viewController: rootNavigation, title: "Your password was changed. Please log in with the new password.")
                }
            } else {
                (self.viewController as! MainViewController).performSegue(withIdentifier: "GOTO_INTIAL_VC", sender: self.viewController)
            }
        }

        @objc(showHelpdesk:)
                func showHelpdesk(command: CDVInvokedUrlCommand) {
                        let msg = command.arguments[0] as? String ?? ""
                        if msg != "" {
                            UserDefaults.standard.set(msg, forKey: "HELPDESK_URL")
                            (self.viewController as! MainViewController).performSegue(withIdentifier: "SHOW_HELPDESK", sender: self.viewController)
                        }
                        }

        @objc(makeZetaPayment:)
                                func makeZetaPayment(command: CDVInvokedUrlCommand) {
                                    let urlStr = command.arguments[0] as? NSString ?? ""

                                                self.command = command

                                    guard let url = URL(string: urlStr as String) else {
                                        return
                                    }

                                    if UIApplication.shared.canOpenURL(url) {
                                        UIApplication.shared.open(url, options: [:]) { (success) in
                                            print(success)
                                        }
                                        NotificationCenter.default.addObserver(self, selector: #selector(self.makePayment(notification:)), name: Notification.Name("DidRecievePayload"), object: nil)
        //                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                    }
                                }

            func makePayment(notification:NSNotification) {
                guard let _ = self.command else {
                                                    return
                                                }

                                let url = notification.userInfo!["payload"] as! NSURL
                                let urlComponents = NSURLComponents(url: url as URL, resolvingAgainstBaseURL: false)
                                let items = (urlComponents?.queryItems)! as [NSURLQueryItem]
                                if (url.scheme == "accentureCafe") {
                                    if let _ = items.first, let _ = items.first?.name, let propertyValue = items.first?.value {
                                        let pluginResult = CDVPluginResult(
                                            status: CDVCommandStatus_OK,
                                            messageAs: propertyValue
                                        )

                                        self.commandDelegate?.run(inBackground: {
                                                                                self.commandDelegate?.send(
                                                                                    pluginResult,
                                                                                    callbackId: self.command!.callbackId
                                                                                )
                                                                            })
                                    }
                                }
            }


        @objc(showWallets:)
        func showWallets(command: CDVInvokedUrlCommand) {
            //(self.viewController as! MainViewController).performSegue(withIdentifier: "SHOW_WALLETS", sender: self.viewController)
            let vc = UIStoryboard.payment.instantiateViewController(withIdentifier: "WalletsVC") as! WalletsVC
                                vc.modusEchoSwift = self
                                (self.viewController as! MainViewController).navigationController?.pushViewController(vc, animated: true)
        }
    
        @objc(showHelp:)
        func showHelp(command: CDVInvokedUrlCommand) {
            (self.viewController as! MainViewController).performSegue(withIdentifier: "SHOW_HELP_CATEGORIES", sender: self.viewController)
        }

        @objc(navigateToLink:)
        func navigateToLink(command : CDVInvokedUrlCommand){
             let link = command.arguments[0] as? String ?? ""
             UIApplication.shared.open(URL(string: link)!, options: [:], completionHandler: nil)
        }

        @objc(showLog:)
                func showLog(command: CDVInvokedUrlCommand) {

                    self.commandDelegate.evalJs("modusechoswift.echo('Plugin Ready!',function(msg) {                            console.log(\"I'm Scotty.P Like Sunith.B, You Know What I'm Sayinnnnn!!!!\");},function(err) {console.log(\"Error\");});")

                }

        @objc(getSimplPayload:)
        func getSimplPayload(command: CDVInvokedUrlCommand) {
            //            guard let mobileNo = KeychainWrapper.standard.string(forKey: "MOBILE_NO") else {return}
            //            guard let email = KeychainWrapper.standard.string(forKey: "EMAIL") else {return}
            //
            //            let user = GSUser(phoneNumber: mobileNo, email: email)
            //            let fp = GSFingerPrint(merchantId: "a7e6e4b8418ad9de1588745ea2bbd3f6", andUser: user)
            //            fp.generateEncryptedPayload { (payload) in

            self.command = command
                        NotificationCenter.default.addObserver(self, selector: #selector(self.returnSimplPayload(notification:)), name: Notification.Name("DidRecieveSimplPayload"), object: nil)

                        Helper.getSimplPayload()
            //                                commandDelegate.run {
            //                                                Helper.getSimplPayload { (payload) in
            //                                                    let pluginResult = CDVPluginResult(
            //                                                        status: CDVCommandStatus_OK,
            //                                                        messageAs: payload
            //
            //                                                    )
            //                                                    self.commandDelegate?.send(
            //                                                        pluginResult,
            //                                                        callbackId: command.callbackId
            //                                                    )
            //                                                }
            //                                            }
                        //            }

        }

        func returnSimplPayload(notification:NSNotification) {
                guard let _ = self.command else {
                    return
                }

                guard let payload = notification.userInfo!["payload"] as? String else {return}
                let pluginResult = CDVPluginResult(
                    status: CDVCommandStatus_OK,
                    messageAs: payload
                )
                self.commandDelegate?.send(
                    pluginResult,
                    callbackId: self.command?.callbackId
                )
            }

        @objc(getAccessToken:)
                func getAccessToken(command: CDVInvokedUrlCommand) {

                    commandDelegate.run {
                                    guard let retrievedToken = KeychainWrapper.standard.string(forKey: "ACCESS_TOKEN") else {
                                        print("Token Error")
                                        return
                                    }

                                    let pluginResult = CDVPluginResult(
                                        status: CDVCommandStatus_OK,
                                        messageAs: retrievedToken
                                    )
                                    self.commandDelegate?.send(
                                        pluginResult,
                                        callbackId: command.callbackId
                                    )
                                }

                    //self.commandDelegate.evalJs("modusechoswift.getToken('" + retrievedToken + "');")

                }

        @objc(goHome)
                func goHome() {

                    self.commandDelegate.evalJs("modusechoswift.goHome(){console.log(\"wasssuppppppp!!!!\");angular.injector(['ng', 'cafeApp']).get(\"Config\").goHome();};")

                }

        @objc(saveAccessToken:)
        func saveAccessToken(accessToken : String) {

            self.commandDelegate.evalJs("modusechoswift.saveAccessToken = function(\(accessToken)) {loginService.saveAccessToken(token);}")

        }

        @objc(updateUserInIonic:)
        func updateUserInIonic(user : [String:Any]) {
            print(user)
            self.commandDelegate.evalJs("modusechoswift.echo.updateUser = function(\(user)) {userService.updateUserFromNative(user)}")

        }
}
